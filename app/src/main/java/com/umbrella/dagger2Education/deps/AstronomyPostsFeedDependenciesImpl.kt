package com.umbrella.dagger2Education.deps

import com.example.all_posts_screen.di.IAstronomyPostsFeedDependencies
import com.example.all_posts_screen.navigate.ICertainPostNavigation
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import javax.inject.Inject

class AstronomyPostsFeedDependenciesImpl @Inject constructor(
    override val astronomyPostsFeedRepository: IAstronomyPostsFeedRepository,
    override val routerCertainPost: ICertainPostNavigation
) : IAstronomyPostsFeedDependencies