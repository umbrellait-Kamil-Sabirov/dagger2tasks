package com.umbrella.dagger2Education.deps

import com.example.certain_post_screen.di.IAstronomyCertainPostDependencies
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository
import javax.inject.Inject

class AstronomyCertainPostDependenciesImpl @Inject constructor(
    override val astronomyPostsCertainRepository: IAstronomyCertainPostRepository
) : IAstronomyCertainPostDependencies