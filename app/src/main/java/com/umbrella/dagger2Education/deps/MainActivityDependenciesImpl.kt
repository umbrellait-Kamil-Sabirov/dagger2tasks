package com.umbrella.dagger2Education.deps

import com.example.main_screen.IAllPostsNavigation
import com.example.main_screen.di.IMainActivityDependencies
import com.example.core_ui.Router
import javax.inject.Inject

class MainActivityDependenciesImpl @Inject constructor(
    override val navigationHolder: Router,
    override val routerAllPosts: IAllPostsNavigation
) : IMainActivityDependencies