package com.umbrella.dagger2Education.navigator

import com.example.all_posts_screen.FragmentAstronomyAllPosts
import com.example.all_posts_screen.navigate.ICertainPostNavigation
import com.example.certain_post_screen.presentation.AstronomyPostViewFragment
import com.example.main_screen.IAllPostsNavigation
import com.example.core_ui.Router
import javax.inject.Inject


class Navigator @Inject constructor(val router: Router) : ICertainPostNavigation,
    IAllPostsNavigation {

    override fun openAllPostsFragment(addToBackStack: Boolean) {
        router.navigateTo(FragmentAstronomyAllPosts.newInstance(), addToBackStack)
    }


    override fun openCertainFragment(date: String, addToBackStack: Boolean) {
        router.navigateTo(AstronomyPostViewFragment.newInstance(date),addToBackStack)
    }
}
