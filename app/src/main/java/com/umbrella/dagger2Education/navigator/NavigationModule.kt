package com.umbrella.dagger2Education.navigator

import com.example.all_posts_screen.navigate.ICertainPostNavigation
import com.example.core.ApplicationScope
import com.example.main_screen.IAllPostsNavigation
import com.example.core_ui.NavigationHolder
import com.example.core_ui.Router
import dagger.Module
import dagger.Provides

@Module
class NavigationModule {

    @Provides
    @ApplicationScope
    fun provideRouter(router: NavigationHolder): Router {
        return router
    }

    @Provides
    @ApplicationScope
    fun provideNavigationHolder() : NavigationHolder {
        return NavigationHolder()
    }

    @Provides
    @ApplicationScope
    fun provideNavigationToCertainPost(router: Router): ICertainPostNavigation {
        return Navigator(router)
    }

    @Provides
    @ApplicationScope
    fun provideNavigationToAllPosts(router: Router): IAllPostsNavigation {
        return Navigator(router)
    }
}