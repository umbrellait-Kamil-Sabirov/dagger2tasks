package com.umbrella.dagger2Education.core

import com.example.core.ApplicationScope
import com.example.all_posts_screen.di.IAstronomyPostsFeedDependencies
import com.example.certain_post_screen.di.IAstronomyCertainPostDependencies
import com.umbrella.dagger2Education.navigator.NavigationModule
import com.example.main_screen.di.IMainActivityDependencies
import com.example.sdk_api.RepositoryFacade
import dagger.Component

@Component(
    modules = [NavigationModule::class, DependenciesModule::class],
    dependencies = [RepositoryFacade::class]
)
@ApplicationScope
interface AppComponent {


    fun mainActivityComponent(): IMainActivityDependencies

    fun astronomyPostsComponent(): IAstronomyPostsFeedDependencies

    fun astronomyCertainPostComponent(): IAstronomyCertainPostDependencies

}
