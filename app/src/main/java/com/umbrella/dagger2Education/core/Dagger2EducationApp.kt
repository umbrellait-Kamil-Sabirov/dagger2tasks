package com.umbrella.dagger2Education.core

import android.app.Application
import com.example.all_posts_screen.di.IAstronomyPostsFeedDependencies
import com.example.all_posts_screen.di.IAstronomyPostsFeedDependenciesProvider
import com.example.certain_post_screen.di.IAstronomyCertainPostDependencies
import com.example.certain_post_screen.di.IAstronomyCertainPostDependenciesProvider
import com.example.main_screen.di.IMainActivityDependencies
import com.example.main_screen.di.IMainScreenDependenciesProvider
import com.example.sdk_api.RepositoryFacade
import com.example.sdk_impl.di.DaggerDataComponent

class Dagger2EducationApp : Application(), IAstronomyPostsFeedDependenciesProvider,
    IAstronomyCertainPostDependenciesProvider,
    IMainScreenDependenciesProvider {
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        val facade: RepositoryFacade = DaggerDataComponent.builder()
            .context(this)
            .build()

        appComponent = DaggerAppComponent.builder()
            .repositoryFacade(facade)
            .build()

    }

    override val dependenciesPosts: IAstronomyPostsFeedDependencies
        get() = appComponent.astronomyPostsComponent()

    override val dependenciesCertain: IAstronomyCertainPostDependencies
        get() = appComponent.astronomyCertainPostComponent()


    override val dependenciesMain: IMainActivityDependencies
        get() = appComponent.mainActivityComponent()
}

