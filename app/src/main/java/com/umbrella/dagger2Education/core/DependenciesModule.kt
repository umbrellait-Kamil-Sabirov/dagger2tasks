package com.umbrella.dagger2Education.core


import com.umbrella.dagger2Education.deps.AstronomyPostsFeedDependenciesImpl
import com.example.all_posts_screen.di.IAstronomyPostsFeedDependencies
import com.example.certain_post_screen.di.IAstronomyCertainPostDependencies
import com.umbrella.dagger2Education.deps.AstronomyCertainPostDependenciesImpl
import com.example.main_screen.di.IMainActivityDependencies
import com.umbrella.dagger2Education.deps.MainActivityDependenciesImpl
import dagger.Binds
import dagger.Module

@Module
interface DependenciesModule {

    @Binds
    fun bindMainActivityDependencies(dependencies: MainActivityDependenciesImpl): IMainActivityDependencies

    @Binds
    fun bindPostsDependencies(dependencies: AstronomyPostsFeedDependenciesImpl): IAstronomyPostsFeedDependencies

    @Binds
    fun bindCertainPostDependencies(dependencies: AstronomyCertainPostDependenciesImpl): IAstronomyCertainPostDependencies
}