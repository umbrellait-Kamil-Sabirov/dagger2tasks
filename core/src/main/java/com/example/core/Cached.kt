package com.example.core

import javax.inject.Qualifier

@Qualifier
@Retention
annotation class Cached