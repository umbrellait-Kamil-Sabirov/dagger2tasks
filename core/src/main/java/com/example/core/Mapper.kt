package com.example.core

interface ToCacheMapper<T> {

    fun map(
        title: String = "",
        date: String = "",
        description: String = "",
        imageUrl: String = ""
    ): T
}

interface Mapper