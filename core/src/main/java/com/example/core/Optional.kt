package com.example.core

data class Optional<T>(val data: T?)