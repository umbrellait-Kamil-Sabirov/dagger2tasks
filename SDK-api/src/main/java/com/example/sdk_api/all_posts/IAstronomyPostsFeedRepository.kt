package com.example.sdk_api.all_posts

import com.example.core_entity.AstronomyPostDto
import io.reactivex.Single

interface IAstronomyPostsFeedRepository {
    fun loadPostsForDates(dates: List<String>): Single<ArrayList<AstronomyPostDto.Base>>
}
