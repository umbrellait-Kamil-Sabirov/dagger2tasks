package com.example.sdk_api

import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository

interface RepositoryFacade {
    fun astronomyPostsFeedRepository(): IAstronomyPostsFeedRepository
    fun astronomyCertainPostRepository(): IAstronomyCertainPostRepository
}