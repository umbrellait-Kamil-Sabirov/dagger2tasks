package com.example.sdk_api.certain_post

import com.example.core_entity.AstronomyPostDto
import io.reactivex.Single

interface IAstronomyCertainPostRepository {

    fun getPostForDate(date: String) : Single<AstronomyPostDto.Base>

}