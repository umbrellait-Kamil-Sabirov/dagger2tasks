package com.example.main_screen

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.core_ui.BaseActivity
import com.example.main_screen.di.DaggerMainActivityComponent
import com.example.main_screen.di.provider
import com.example.core_ui.Router
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: MainViewModel by viewModels {
        viewModelFactory
    }

    @Inject
    lateinit var navigator: IAllPostsNavigation

    @Inject
    lateinit var navigationHolder: Router

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerMainActivityComponent.builder()
            .iMainActivityDependencies(this.provider.dependenciesMain)
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_activityy)
        navigationHolder.setUp(supportFragmentManager, R.id.frame_layout_main_fragment_container)

        viewModel.navigateToAstronomyPostsFeedCommand.observe(this) {
            navigator.openAllPostsFragment(true)
        }
        viewModel.startFlow()
    }


    override fun onDestroy() {
        super.onDestroy()
        navigationHolder.release()
    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

}
