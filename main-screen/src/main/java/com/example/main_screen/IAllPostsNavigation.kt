package com.example.main_screen

interface IAllPostsNavigation {

    fun openAllPostsFragment(addToBackStack: Boolean)

}