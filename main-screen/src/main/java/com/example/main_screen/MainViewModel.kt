package com.example.main_screen

import com.example.core_ui.BaseViewModel
import com.example.core_ui.OneShotLiveData
import javax.inject.Inject

class MainViewModel @Inject constructor(): com.example.core_ui.BaseViewModel() {

    val navigateToAstronomyPostsFeedCommand = com.example.core_ui.OneShotLiveData<Unit>()

    fun startFlow() {
        navigateToAstronomyPostsFeedCommand.postValue(Unit)
    }
}