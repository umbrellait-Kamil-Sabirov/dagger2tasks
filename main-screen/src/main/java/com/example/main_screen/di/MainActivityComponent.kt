package com.example.main_screen.di

import android.app.Application
import android.content.Context
import com.example.core.FeatureScope
import com.example.main_screen.MainActivity
import dagger.Component


@Component(
    modules = [MainActivityModule::class],
    dependencies = [IMainActivityDependencies::class]
)
@FeatureScope
interface MainActivityComponent {

    fun inject(activity: MainActivity)

}

interface IMainScreenDependenciesProvider {
    val dependenciesMain: IMainActivityDependencies
}

val Context.provider: IMainScreenDependenciesProvider
    get() = when (this) {
        is IMainScreenDependenciesProvider -> this
        is Application -> error("error")
        else -> applicationContext.provider
    }