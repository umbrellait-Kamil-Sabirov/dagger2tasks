package com.example.main_screen.di

import com.example.main_screen.IAllPostsNavigation
import com.example.core_ui.Router


interface IMainActivityDependencies {
    val navigationHolder: Router
    val routerAllPosts: IAllPostsNavigation
}