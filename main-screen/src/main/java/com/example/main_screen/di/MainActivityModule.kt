package com.example.main_screen.di

import androidx.lifecycle.ViewModel
import com.example.core.ViewModelFactoryModule
import com.example.core.ViewModelKey
import com.example.main_screen.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindsMainViewModel(viewModel: MainViewModel) : ViewModel
}