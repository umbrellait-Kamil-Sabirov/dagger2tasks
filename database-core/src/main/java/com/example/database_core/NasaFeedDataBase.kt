package com.example.database_core

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [AstronomyPostEntity.Base::class], version = 1)
abstract class NasaFeedDataBase : RoomDatabase() {

    abstract fun astronomyPostsDao(): AstronomyPostsDAO

}