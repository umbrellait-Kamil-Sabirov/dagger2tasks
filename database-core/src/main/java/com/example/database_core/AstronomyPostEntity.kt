package com.example.database_core

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.core.ToCacheMapper

interface AstronomyPostEntity {
    fun <T> map(mapper: ToCacheMapper<T>): T

    @Entity(tableName = DataBaseConfiguration.ASTRONOMY_POSTS_TABLE_NAME)
    data class Base(

        @field:ColumnInfo(name = "title")
        var title: String = "",

        @field:ColumnInfo(name = "date")
        @field:PrimaryKey
        var date: String = "",

        @field:ColumnInfo(name = "description")
        var description: String = "",

        @field:ColumnInfo(name = "image_url")
        var imageUrl: String = ""
    ) : AstronomyPostEntity {
        override fun <T> map(mapper: ToCacheMapper<T>): T {
            return mapper.map(title, date, description, imageUrl)
        }
    }

}