package com.example.database_core

import android.content.Context
import androidx.room.Room
import com.example.core.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Qualifier


@Module
class DatabaseModule {

    @Provides
    @TableName
    @ApplicationScope
    fun provideDatabaseName(): String = DataBaseConfiguration.ASTRONOMY_POSTS_TABLE_NAME


    @Provides
    @ApplicationScope
    fun provideNasaFeedDataBase(
        context: Context,
        @TableName
        databaseName: String
    ): NasaFeedDataBase =
        Room.databaseBuilder(
            context,
            NasaFeedDataBase::class.java,
            databaseName
        ).build()

    @Provides
    @ApplicationScope
    fun providePostsDao(nasaFeedDataBase: NasaFeedDataBase): AstronomyPostsDAO =
        nasaFeedDataBase.astronomyPostsDao()

}

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class TableName