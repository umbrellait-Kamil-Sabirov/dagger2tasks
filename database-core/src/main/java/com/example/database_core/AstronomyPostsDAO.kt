package com.example.database_core

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface AstronomyPostsDAO {

    @Query("SELECT * FROM " + DataBaseConfiguration.ASTRONOMY_POSTS_TABLE_NAME)
    fun getAllPosts(): Flowable<List<AstronomyPostEntity.Base>>

    @Query("SELECT * FROM " + DataBaseConfiguration.ASTRONOMY_POSTS_TABLE_NAME + " WHERE date == :date")
    fun getPostForDate(date: String): Single<AstronomyPostEntity.Base>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: AstronomyPostEntity.Base)

    @Query("DELETE FROM " + DataBaseConfiguration.ASTRONOMY_POSTS_TABLE_NAME)
    fun deleteAll()

}