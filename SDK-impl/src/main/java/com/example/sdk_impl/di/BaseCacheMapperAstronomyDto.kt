package com.example.sdk_impl.di

import com.example.core.ToCacheMapper
import com.example.core_entity.AstronomyPostDto

class BaseCacheMapperAstronomyDto : ToCacheMapper<AstronomyPostDto.Base> {
    override fun map(
        title: String,
        date: String,
        description: String,
        imageUrl: String
    ): AstronomyPostDto.Base {
        return AstronomyPostDto.Base(title, date, description, imageUrl)
    }

}