package com.example.sdk_impl.di

import com.example.core.ApplicationScope
import com.example.core.Cached
import com.example.network_core.IAstronomyPostsDataSource
import com.example.core.Remote
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository
import com.example.sdk_impl.all_posts.AstronomyPostsFeedRepository
import com.example.sdk_impl.certain_post.AstronomyCertainPostRepository
import com.example.sdk_impl.di.DataModule
import dagger.Module
import dagger.Provides

@Module(includes = [DataModule::class])
class DomainModule {

    @Provides
    @ApplicationScope
    fun provideAstronomyPostsRepository(@Remote dataSource: IAstronomyPostsDataSource): IAstronomyPostsFeedRepository =
        AstronomyPostsFeedRepository(dataSource)


    @Provides
    @ApplicationScope
    fun provideAstronomyCertainPostRepository(@Cached dataSource: IAstronomyPostsDataSource) : IAstronomyCertainPostRepository =
        AstronomyCertainPostRepository(dataSource)

}