package com.example.sdk_impl.di

import com.example.core.ApplicationScope
import com.example.database_core.DatabaseModule
import com.example.network_core.IAstronomyPostsDataSource
import com.example.core.Cached
import com.example.core.Remote
import com.example.core.ToCacheMapper
import com.example.core_entity.AstronomyPostDto
import com.example.database_core.AstronomyPostEntity
import com.example.database_core.AstronomyPostsDAO
import com.example.network_core.*
import com.example.sdk_impl.certain_post.CachedAstronomyPostsDataSource
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class, DatabaseModule::class, NasaServiceModule::class])
class DataModule {

    @Provides
    @ApplicationScope
    @Cached
    fun provideCachedAstronomyPostsDataSource(
        @Remote remoteDataSource: IAstronomyPostsDataSource,
        localStorage: AstronomyPostsDAO,
        cacheMapper: ToCacheMapper<AstronomyPostDto.Base>,
        mapper: ToCacheMapper<AstronomyPostEntity.Base>
    ): IAstronomyPostsDataSource =
        CachedAstronomyPostsDataSource(
            remoteDataSource,
            localStorage,
            cacheMapper,
            mapper
        )


    @Provides
    @ApplicationScope
    fun provideToCacheMapperDto() : ToCacheMapper<AstronomyPostDto.Base>{
        return BaseCacheMapperAstronomyDto()
    }

    @Provides
    @ApplicationScope
    fun provideToCacheMapperEntity() : ToCacheMapper<AstronomyPostEntity.Base>{
        return BaseCacheMapperEntity()
    }


    @Provides
    @ApplicationScope
    @Remote
    fun provideNasaService(
        nasaServiceApi: INasaServiceApi,
        @ApiKey apiKey: String
    ): IAstronomyPostsDataSource =
        NasaCloudDataSource(nasaServiceApi, apiKey)

}