package com.example.sdk_impl.di

import android.content.Context
import com.example.core.ApplicationScope
import com.example.sdk_api.RepositoryFacade
import dagger.BindsInstance
import dagger.Component

@Component(modules = [DomainModule::class])
@ApplicationScope
interface DataComponent : RepositoryFacade {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun context(context: Context) : Builder

        fun build() : DataComponent
    }
}