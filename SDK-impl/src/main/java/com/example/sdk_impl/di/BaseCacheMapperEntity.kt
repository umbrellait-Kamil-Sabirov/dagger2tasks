package com.example.sdk_impl.di

import com.example.core.ToCacheMapper
import com.example.database_core.AstronomyPostEntity

class BaseCacheMapperEntity : ToCacheMapper<AstronomyPostEntity.Base> {
    override fun map(
        title: String,
        date: String,
        description: String,
        imageUrl: String
    ): AstronomyPostEntity.Base {
        return AstronomyPostEntity.Base(title, date, description, imageUrl)
    }

}