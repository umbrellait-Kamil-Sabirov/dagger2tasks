package com.example.sdk_impl.certain_post

import com.example.core_entity.AstronomyPostDto
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository
import com.example.network_core.IAstronomyPostsDataSource
import io.reactivex.Single

class AstronomyCertainPostRepository(private val postsDataSource: IAstronomyPostsDataSource) :
    IAstronomyCertainPostRepository {
    override fun getPostForDate(date: String): Single<AstronomyPostDto.Base> {
        return postsDataSource.getAstronomyPost(date)
    }
}