package com.example.sdk_impl.certain_post

import com.example.core.ToCacheMapper
import com.example.core_entity.AstronomyPostDto
import com.example.database_core.AstronomyPostEntity
import com.example.network_core.IAstronomyPostsDataSource
import com.example.database_core.AstronomyPostsDAO
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class CachedAstronomyPostsDataSource constructor(
    private val remoteDataSource: IAstronomyPostsDataSource,
    private val localStorage: AstronomyPostsDAO,
    private val cacheMapper: ToCacheMapper<AstronomyPostDto.Base>,
    private val mapper: ToCacheMapper<AstronomyPostEntity.Base>
) : IAstronomyPostsDataSource {

    override fun getAstronomyPost(date: String): Single<AstronomyPostDto.Base> {
        return localStorage.getPostForDate(date).map {
            it.map(cacheMapper)
        }
            .onErrorResumeNext {
                getFromRemoteSource(date)
            }
            .subscribeOn(Schedulers.io())
    }


    private fun getFromRemoteSource(date: String): Single<AstronomyPostDto.Base> {
        return remoteDataSource
            .getAstronomyPost(date)
            .doOnSuccess {
                localStorage.insert(
                    it.map(mapper)
                )
            }
    }
}