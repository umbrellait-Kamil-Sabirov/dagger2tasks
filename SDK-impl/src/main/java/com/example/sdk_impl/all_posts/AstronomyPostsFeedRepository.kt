package com.example.sdk_impl.all_posts

import com.example.core.Optional
import com.example.core_entity.AstronomyPostDto
import com.example.database_core.AstronomyPostEntity
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import com.example.network_core.IAstronomyPostsDataSource
import io.reactivex.Single
import io.reactivex.rxkotlin.toObservable
import java.io.IOException
import kotlin.collections.ArrayList

class AstronomyPostsFeedRepository(
    private val postsDataSource: IAstronomyPostsDataSource
) : IAstronomyPostsFeedRepository {

    override fun loadPostsForDates(dates: List<String>): Single<ArrayList<AstronomyPostDto.Base>> {
        return dates.toObservable()
            .concatMapSingle { date ->
                postsDataSource.getAstronomyPost(date)
                    .map(::Optional)
                    .onErrorReturnItem(Optional(null))
            }
            .collectInto(ArrayList()) { list: ArrayList<AstronomyPostDto.Base>, item ->
                item?.data?.let(list::add)
            }
            .flatMap { list ->
                if (list.isNotEmpty()) {
                    Single.just(list)
                } else {
                    Single.error(
                        IOException(
                            "Loading posts for selected dates failed"
                        )
                    )
                }
            }
    }


}
