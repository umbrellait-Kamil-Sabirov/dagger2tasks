package com.example.certain_post_screen.presentation

import androidx.lifecycle.MutableLiveData
import com.example.core_entity.AstronomyPostDto
import com.example.core_ui.BaseViewModel
import com.example.core_ui.OneShotLiveData
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class AstronomyPostViewModel constructor(
    private val repository: IAstronomyCertainPostRepository,
    private val data: String
) : BaseViewModel() {


    private val shareSubject = PublishSubject.create<Unit>()

    private val disposables = CompositeDisposable()

    val postDetailsState = MutableLiveData<AstronomyPostDto.Base>()
    val shareCommand = OneShotLiveData<AstronomyPostDto.Base>()

    fun fetchData() {
        val postSingle = repository.getPostForDate(data).cache()

        postSingle
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = ::presentPost,
                onError = {}
            )
            .addTo(disposables)

        shareSubject
            .throttleFirst(300L, TimeUnit.MILLISECONDS)
            .switchMapSingle { postSingle }
            .subscribeBy(
                onNext = { post ->
                    shareCommand.postValue(post)
                },
                onError = {}
            )
            .addTo(disposables)
    }

    private fun presentPost(post: AstronomyPostDto.Base) {
        postDetailsState.postValue(post)
    }

    fun onShareButtonClicked() {
        shareSubject.onNext(Unit)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}