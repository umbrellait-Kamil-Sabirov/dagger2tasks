package com.example.certain_post_screen.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject


class AstronomyPostViewModelFactory @AssistedInject constructor(
    private val repository: IAstronomyCertainPostRepository,
    @Assisted("data") private val data: String
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        return AstronomyPostViewModel(repository, data) as T
    }
}

@AssistedFactory
interface Factory {
    fun create(@Assisted("data") data: String): AstronomyPostViewModelFactory
}
