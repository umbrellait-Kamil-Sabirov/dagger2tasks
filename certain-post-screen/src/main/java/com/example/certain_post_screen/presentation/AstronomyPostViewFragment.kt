package com.example.certain_post_screen.presentation

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.example.certain_post_screen.R
import com.example.certain_post_screen.databinding.FragmentAstronomyPostBinding
import com.example.certain_post_screen.di.DaggerAstronomyCertainPostComponent
import com.example.certain_post_screen.di.provider
import com.example.core_entity.AstronomyPostDto
import com.example.core_ui.BaseActivity

import javax.inject.Inject


class AstronomyPostViewFragment : Fragment() {

    lateinit var binding: FragmentAstronomyPostBinding

    companion object {

        private const val KEY_DATE = "key_date"

        fun newInstance(date: String): AstronomyPostViewFragment =
            AstronomyPostViewFragment().apply {
                arguments = bundleOf(KEY_DATE to date)
            }
    }

    private val date: String by lazy {
        arguments?.getString(KEY_DATE).orEmpty()
    }


    @Inject
    lateinit var viewModelFactory: Factory


    private val viewModel: AstronomyPostViewModel by viewModels {
        viewModelFactory.create(date)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAstronomyCertainPostComponent.builder()
            .iAstronomyCertainPostDependencies(requireContext().provider.dependenciesCertain)
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.fetchData()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAstronomyPostBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        viewModel.postDetailsState.observe(viewLifecycleOwner, ::setPostDetailsData)
        viewModel.shareCommand.observe(viewLifecycleOwner, ::sharePost)
    }

    private fun setupActionBar() {
        activity?.let { it as? BaseActivity }
            ?.supportActionBar
            ?.apply {
                setHomeButtonEnabled(true)
                setDisplayHomeAsUpEnabled(true)
            }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_stronomy_picture_post, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.share -> {
                viewModel.onShareButtonClicked()
                true
            }
            else -> super.onOptionsItemSelected(item)

        }
    }

    private fun setPostDetailsData(post: AstronomyPostDto.Base) {
        binding.apply {
            textViewAstronomyPostTitle.text = post.title
            textViewAstronomyPostDate.text = post.date
            textViewAstronomyPostDescription.text = post.description
            Glide.with(imageViewAstronomyPostImage)
                .load(post.imageUrl)
                .into(imageViewAstronomyPostImage)
        }
    }


    private fun sharePost(post: AstronomyPostDto.Base) {
        val intent = Intent(Intent.ACTION_SEND)
        val text = "${post.title} \n ${post.description} \n ${post.imageUrl}"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        intent.type = "text/*"
        startActivity(intent)
    }
}