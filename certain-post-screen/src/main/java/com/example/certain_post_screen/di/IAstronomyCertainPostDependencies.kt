package com.example.certain_post_screen.di

import com.example.sdk_api.certain_post.IAstronomyCertainPostRepository

interface IAstronomyCertainPostDependencies {
    val astronomyPostsCertainRepository: IAstronomyCertainPostRepository
}