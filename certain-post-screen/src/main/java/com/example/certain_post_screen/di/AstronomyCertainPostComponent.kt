package com.example.certain_post_screen.di

import android.content.Context
import com.example.certain_post_screen.presentation.AstronomyPostViewFragment
import com.example.core.FeatureScope
import dagger.Component


@Component(
    dependencies = [IAstronomyCertainPostDependencies::class],
    modules = [AstronomyCertainPostModule::class]
)
@FeatureScope
interface AstronomyCertainPostComponent {

    fun inject(fragment: AstronomyPostViewFragment)

}

interface IAstronomyCertainPostDependenciesProvider {

    val dependenciesCertain: IAstronomyCertainPostDependencies

}


val Context.provider: IAstronomyCertainPostDependenciesProvider
    get() = when (this) {
        is IAstronomyCertainPostDependenciesProvider -> this
        else -> applicationContext.provider
    }
