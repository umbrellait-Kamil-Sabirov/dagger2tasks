package com.example.certain_post_screen.di

import androidx.lifecycle.ViewModel
import com.example.certain_post_screen.presentation.AstronomyPostViewModel
import com.example.core.ViewModelFactoryModule
import com.example.core.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class AstronomyCertainPostModule {

    @Binds
    @IntoMap
    @ViewModelKey(AstronomyPostViewModel::class)
    abstract fun bindCertainPost(viewModel: AstronomyPostViewModel): ViewModel

}