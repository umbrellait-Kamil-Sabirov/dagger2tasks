package com.example.all_posts_screen.navigate


interface ICertainPostNavigation  {

    fun openCertainFragment(date: String, addToBackStack: Boolean)

}