package com.example.all_posts_screen.presentation

data class NotifyListUpdateData(
    val index : Int,
    val size : Int
)