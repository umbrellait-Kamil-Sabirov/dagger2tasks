package com.example.all_posts_screen.presentation.adapter

import com.example.all_posts_screen.databinding.ItemAstronomyPostBinding
import com.example.all_posts_screen.presentation.AstronomyPostsAdapterContract
import com.example.all_posts_screen.presentation.BaseAstronomyPostViewHolder

class AstronomyPostItemViewHolder(
    private val item: ItemAstronomyPostBinding,
    private val postPresenter: AstronomyPostsAdapterContract.PostPresenter
) : BaseAstronomyPostViewHolder(item), AstronomyPostsAdapterContract.PostView {

    init {
        item.root.setOnClickListener { postPresenter.onItemClick(adapterPosition) }
    }

    override fun setTitle(title: String) {
        item.textViewItemAstronomyPictureTitle.text = title
    }

    override fun setDate(date: String) {
        item.textViewItemAstronomyPictureDate.text = date
    }

    override fun setDescription(description: String) {
        item.textViewItemAstronomyPictureDescription.text = description
    }

}