package com.example.all_posts_screen.presentation

import androidx.lifecycle.MutableLiveData
import com.example.core_entity.AstronomyPostDto
import com.example.core_ui.BaseViewModel
import com.example.core_ui.IShouldLoadListener
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class AstronomyPostsFeedViewModel @Inject constructor(private val repository: IAstronomyPostsFeedRepository) :
    BaseViewModel(), AstronomyPostsAdapterContract.AdapterPresenter,
    AstronomyPostsAdapterContract.PostPresenter, AstronomyPostsAdapterContract.LoadingPresenter,
    IShouldLoadListener {

    companion object {
        private const val POSTS_LOADING_PACK_SIZE = 10
    }

    private val disposables = CompositeDisposable()

    private val astronomyPostsList = ArrayList<AstronomyPostDto.Base>()

    private var isLoading = false

    private val cursorCalendar = Calendar.getInstance()
    private val postsDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

    val navigateToPostCommand = com.example.core_ui.OneShotLiveData<String>()
    val retryButtonVisibleState = MutableLiveData<Boolean>()
    val notifyItemRangeInsertedCommand = com.example.core_ui.OneShotLiveData<NotifyListUpdateData>()
    val notifyItemRangeRemovedCommand = com.example.core_ui.OneShotLiveData<NotifyListUpdateData>()

    fun fetchData() {
        loadNextPosts()
    }

    override fun getAstronomyItemsCount(): Int =
        if (isLoading) astronomyPostsList.size + 1 else astronomyPostsList.size

    override fun getAstronomyItemViewType(position: Int): Int {
        return if (position == astronomyPostsList.size)
            AstronomyPostsAdapterContract.VIEW_TYPE_LOADING
        else
            AstronomyPostsAdapterContract.VIEW_TYPE_POST
    }


    override fun onBindAstronomyPostItemView(
        postView: AstronomyPostsAdapterContract.PostView,
        position: Int
    ) {
        val item = astronomyPostsList[position]
        with(postView) {
            setTitle(item.title)
            setDate(item.date)
            setDescription(item.description)
        }
    }

    override fun onBindAstronomyLoadingItemView(
        loadingView: AstronomyPostsAdapterContract.LoadingView,
        position: Int
    ) {
        loadingView.setLoading(isLoading)
    }

    private fun loadNextPosts() {
        val datesToLoadPosts = ArrayList<String>()
        for (day in 0 until POSTS_LOADING_PACK_SIZE) {
            datesToLoadPosts.add(postsDateFormat.format(cursorCalendar.time))
            cursorCalendar.add(Calendar.DATE, -1)
        }
        cursorCalendar.add(Calendar.DATE, POSTS_LOADING_PACK_SIZE)

        repository.loadPostsForDates(datesToLoadPosts)
            .doOnSubscribe {
                setIsLoading(true)
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    onPostsLoaded(it)
                },
                onError = { onPostsLoadingFailed() }
            )
            .addTo(disposables)
    }

    private fun setIsLoading(shouldStartLoading: Boolean) {
        isLoading = shouldStartLoading
        if (isLoading) notifyItemRangeInsertedCommand.value =
            NotifyListUpdateData(astronomyPostsList.size, 1)
        else notifyItemRangeRemovedCommand.value = NotifyListUpdateData(astronomyPostsList.size, 1)
    }

    private fun onPostsLoaded(posts: ArrayList<AstronomyPostDto.Base>) {
        setIsLoading(false)
        if (astronomyPostsList.isEmpty() && posts.isNotEmpty()) retryButtonVisibleState.value =
            false
        val position = astronomyPostsList.size
        astronomyPostsList.addAll(posts)
        notifyItemRangeInsertedCommand.value = NotifyListUpdateData(position, posts.size)
        cursorCalendar.add(Calendar.DATE, -POSTS_LOADING_PACK_SIZE)
    }

    private fun onPostsLoadingFailed() {
        setIsLoading(false)
        if (astronomyPostsList.isEmpty()) retryButtonVisibleState.value = true
    }

    override fun onShouldLoadMore() {
        if (!isLoading) {
            loadNextPosts()
        }
    }

    override fun onItemClick(position: Int) {
        navigateToPostCommand.value = astronomyPostsList[position].date
    }

    fun onRetryLoadingClicked() = onShouldLoadMore()


}