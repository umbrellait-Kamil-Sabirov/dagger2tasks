package com.example.all_posts_screen.presentation.adapter

import android.view.View
import com.example.all_posts_screen.databinding.ItemLoadingProgressBarBinding
import com.example.all_posts_screen.presentation.AstronomyPostsAdapterContract
import com.example.all_posts_screen.presentation.BaseAstronomyPostViewHolder

class AstronomyPostLoadingViewHolder(
    private val item: ItemLoadingProgressBarBinding
) : BaseAstronomyPostViewHolder(item), AstronomyPostsAdapterContract.LoadingView {

    override fun setLoading(isLoading: Boolean) {
        item.progressBarLoadingItem.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

}