package com.example.all_posts_screen.presentation

interface AstronomyPostsAdapterContract {

    companion object {
        const val VIEW_TYPE_POST = 0
        const val VIEW_TYPE_LOADING = 1
    }

    interface AdapterPresenter {

        fun getAstronomyItemsCount(): Int

        fun getAstronomyItemViewType(position: Int): Int

        fun onBindAstronomyPostItemView(postView: PostView, position: Int)

        fun onBindAstronomyLoadingItemView(loadingView: LoadingView, position: Int)

    }

    interface PostPresenter {

        fun onItemClick(position: Int)

    }

    interface LoadingPresenter

    interface PostView {

        fun setTitle(title: String)

        fun setDate(date: String)

        fun setDescription(description: String)

    }

    interface LoadingView {

        fun setLoading(isLoading: Boolean)
    }

}

