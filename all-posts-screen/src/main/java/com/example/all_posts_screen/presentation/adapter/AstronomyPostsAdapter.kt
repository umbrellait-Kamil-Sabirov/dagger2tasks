package com.example.all_posts_screen.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.all_posts_screen.databinding.ItemAstronomyPostBinding
import com.example.all_posts_screen.databinding.ItemLoadingProgressBarBinding
import com.example.all_posts_screen.presentation.AstronomyPostsAdapterContract
import com.example.all_posts_screen.presentation.BaseAstronomyPostViewHolder

class AstronomyPostsAdapter(
    private val adapterPresenter: AstronomyPostsAdapterContract.AdapterPresenter,
    private val postPresenter: AstronomyPostsAdapterContract.PostPresenter
) : RecyclerView.Adapter<BaseAstronomyPostViewHolder>() {


    override fun getItemCount(): Int = adapterPresenter.getAstronomyItemsCount()

    override fun getItemViewType(position: Int): Int =
        adapterPresenter.getAstronomyItemViewType(position)

    override fun onBindViewHolder(holderItem: BaseAstronomyPostViewHolder, position: Int) {
        when (holderItem) {
            is AstronomyPostItemViewHolder -> adapterPresenter.onBindAstronomyPostItemView(
                holderItem,
                position
            )
            is AstronomyPostLoadingViewHolder -> adapterPresenter.onBindAstronomyLoadingItemView(
                holderItem,
                position
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAstronomyPostViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemAstronomyPost = ItemAstronomyPostBinding.inflate(inflater, parent, false)
        val itemAstronomyLoading = ItemLoadingProgressBarBinding.inflate(inflater, parent, false)
        return when (viewType) {
            AstronomyPostsAdapterContract.VIEW_TYPE_POST -> AstronomyPostItemViewHolder(
                itemAstronomyPost,
                postPresenter
            )
            AstronomyPostsAdapterContract.VIEW_TYPE_LOADING -> AstronomyPostLoadingViewHolder(
                itemAstronomyLoading
            )
            else -> AstronomyPostItemViewHolder(
                itemAstronomyPost,
                postPresenter
            )
        }

    }


}