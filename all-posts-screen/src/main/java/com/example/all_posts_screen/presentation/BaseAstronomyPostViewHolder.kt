package com.example.all_posts_screen.presentation

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding


abstract class BaseAstronomyPostViewHolder(itemView: ViewBinding) : RecyclerView.ViewHolder(itemView.root)

