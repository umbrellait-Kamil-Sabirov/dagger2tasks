package com.example.all_posts_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.all_posts_screen.databinding.FragmentAstronomyAllPostsBinding
import com.example.all_posts_screen.di.DaggerAstronomyPostsComponent
import com.example.all_posts_screen.di.provider
import com.example.all_posts_screen.navigate.ICertainPostNavigation
import com.example.all_posts_screen.presentation.AstronomyPostsFeedViewModel
import com.example.all_posts_screen.presentation.adapter.AstronomyPostsAdapter


import javax.inject.Inject

class FragmentAstronomyAllPosts : Fragment() {

    companion object {
        fun newInstance() = FragmentAstronomyAllPosts()
    }

    private lateinit var binding: FragmentAstronomyAllPostsBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var router: ICertainPostNavigation

    private val viewModel: AstronomyPostsFeedViewModel by viewModels {
        viewModelFactory
    }

    private val astronomyPostsAdapter: AstronomyPostsAdapter by lazy {
        AstronomyPostsAdapter(
            viewModel,
            viewModel
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAstronomyPostsComponent.builder()
            .iAstronomyPostsFeedDependencies(requireContext().provider.dependenciesPosts)
            .build().inject(this)
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.fetchData()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentAstronomyAllPostsBinding.inflate(inflater, container, false)
        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupActionBar()
        initRecyclerView()
        binding.buttonAstronomyPostsLoadingRetry.setOnClickListener {
            viewModel.onRetryLoadingClicked()
        }

        viewModel.navigateToPostCommand.observe(viewLifecycleOwner) { date ->
            router.openCertainFragment(date, true)
        }

        viewModel.retryButtonVisibleState.observe(viewLifecycleOwner) { visible ->
            binding.buttonAstronomyPostsLoadingRetry.visibility =
                if (visible) View.VISIBLE else View.GONE
        }

        viewModel.notifyItemRangeInsertedCommand.observe(viewLifecycleOwner) { data ->
            astronomyPostsAdapter.notifyItemRangeInserted(data.index, data.size)
        }

        viewModel.notifyItemRangeRemovedCommand.observe(viewLifecycleOwner) { data ->
            astronomyPostsAdapter.notifyItemRangeRemoved(data.index, data.size)
        }
    }

    private fun setupActionBar() {
        activity?.let { it as? com.example.core_ui.BaseActivity }
            ?.supportActionBar
            ?.apply {
                setHomeButtonEnabled(false)
                setDisplayHomeAsUpEnabled(false)
            }
    }

    private fun initRecyclerView() {
        with(binding.recyclerViewAstronomyPosts) {
            adapter = astronomyPostsAdapter
            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            val scrollListener =
                com.example.core_ui.LoadingRecyclerViewScrollListener(
                    linearLayoutManager,
                    viewModel,
                    21
                )
            addOnScrollListener(scrollListener)
        }
    }
}