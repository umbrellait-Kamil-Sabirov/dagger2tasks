package com.example.all_posts_screen.di

import androidx.lifecycle.ViewModel
import com.example.all_posts_screen.presentation.AstronomyPostsFeedViewModel
import com.example.core.ViewModelFactoryModule
import com.example.core.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class AstronomyPostsModule {

    @Binds
    @IntoMap
    @ViewModelKey(AstronomyPostsFeedViewModel::class)
    abstract fun bindAstronomyPosts(viewModel: AstronomyPostsFeedViewModel): ViewModel

}