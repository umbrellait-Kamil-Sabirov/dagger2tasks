package com.example.all_posts_screen.di

import android.content.Context
import androidx.fragment.app.Fragment
import com.example.all_posts_screen.FragmentAstronomyAllPosts
import com.example.core.FeatureScope
import dagger.Component

@Component(
    modules = [AstronomyPostsModule::class],
    dependencies = [IAstronomyPostsFeedDependencies::class]
)
@FeatureScope
interface AstronomyPostsComponent {

    fun inject(fragment: FragmentAstronomyAllPosts)

}

interface IAstronomyPostsFeedDependenciesProvider {
    val dependenciesPosts: IAstronomyPostsFeedDependencies
}

val Context.provider: IAstronomyPostsFeedDependenciesProvider
    get() = when (this) {
        is IAstronomyPostsFeedDependenciesProvider -> this
        else -> applicationContext.provider
    }