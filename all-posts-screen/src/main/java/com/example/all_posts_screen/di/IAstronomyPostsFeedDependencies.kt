package com.example.all_posts_screen.di

import com.example.all_posts_screen.navigate.ICertainPostNavigation
import com.example.sdk_api.all_posts.IAstronomyPostsFeedRepository


interface IAstronomyPostsFeedDependencies {
    val astronomyPostsFeedRepository : IAstronomyPostsFeedRepository
    val routerCertainPost: ICertainPostNavigation
}