package com.example.network_core

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface INasaServiceApi {

    companion object {
        const val BASE_URL = "https://api.nasa.gov"
        const val PLANETARY_APOD_PATH = "planetary/apod"
    }

    @GET(PLANETARY_APOD_PATH)
    fun getAstronomyPost(
        @Query("api_key") apiKey: String,
        @Query("date") date: String
    ): Single<AstronomyPostResponse>
}