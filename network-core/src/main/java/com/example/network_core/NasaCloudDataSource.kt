package com.example.network_core


import com.example.core_entity.AstronomyPostDto
import io.reactivex.Single

class NasaCloudDataSource(
    private val nasaServiceApi: INasaServiceApi,
    private val apiKey: String
) : IAstronomyPostsDataSource {

    override fun getAstronomyPost(date: String): Single<AstronomyPostDto.Base> {
        return nasaServiceApi.getAstronomyPost(apiKey, date)
            .map { dto ->
                AstronomyPostDto.Base(
                    title = dto.title,
                    date = dto.date,
                    description = dto.explanation,
                    imageUrl = dto.hdUrl
                )
            }
    }
}