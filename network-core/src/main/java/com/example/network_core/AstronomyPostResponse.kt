package com.example.network_core

import com.fasterxml.jackson.annotation.JsonProperty

class AstronomyPostResponse {

    @JsonProperty("date")
    var date: String = ""

    @JsonProperty("explanation")
    var explanation: String = ""

    @JsonProperty("hdurl")
    var hdUrl: String = ""

    @JsonProperty("media_type")
    var media_type: String = ""

    @JsonProperty("service_version")
    var service_version: String = ""

    @JsonProperty("title")
    var title: String = ""

    @JsonProperty("url")
    var url: String = ""

}