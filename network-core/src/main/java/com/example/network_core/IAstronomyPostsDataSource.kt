package com.example.network_core

import com.example.core_entity.AstronomyPostDto
import io.reactivex.Single

interface IAstronomyPostsDataSource {
    fun getAstronomyPost(date: String): Single<AstronomyPostDto.Base>
}