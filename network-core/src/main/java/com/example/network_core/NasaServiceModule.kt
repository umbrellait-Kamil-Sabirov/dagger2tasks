package com.example.network_core

import android.content.Context
import com.example.core.ApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Qualifier


@Module
class NasaServiceModule {

    @Provides
    @ApplicationScope
    fun provideRetrofitInstance(
        client: OkHttpClient,
        converterFactory: JacksonConverterFactory
    ): Retrofit = Retrofit.Builder()
        .baseUrl(com.example.network_core.INasaServiceApi.BASE_URL)
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
        .addConverterFactory(converterFactory)
        .build()

    @Provides
    @ApplicationScope
    fun provideServiceApi(retrofit: Retrofit): INasaServiceApi =
        retrofit.create(INasaServiceApi::class.java)


    @Provides
    @ApplicationScope
    @ApiKey
    fun provideApiKey(context: Context): String = ApiConfiguration.API_KEY


}
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApiKey