package com.example.core_entity

import com.example.core.ToCacheMapper


interface AstronomyPostDto {
    fun <T> map(mapper: ToCacheMapper<T>): T
    data class Base(
        var title: String = "",
        var date: String = "",
        var description: String = "",
        var imageUrl: String = ""
    ) : AstronomyPostDto {
        override fun <T> map(mapper: ToCacheMapper<T>): T {
            return mapper.map(title, date, description, imageUrl)
        }
    }
}