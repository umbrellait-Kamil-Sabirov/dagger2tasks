package com.example.core_ui

interface IShouldLoadListener {

    fun onShouldLoadMore()
}