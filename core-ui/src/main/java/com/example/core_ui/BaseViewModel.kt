package com.example.core_ui

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()