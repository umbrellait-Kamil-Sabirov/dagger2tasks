package com.example.core_ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction


class NavigationHolder : Router {

    private var fragmentManager: FragmentManager? = null
    private var containerId: Int? = null

    override fun setUp(
        fragmentManager: FragmentManager,
        containerId: Int
    ) {
        this.fragmentManager = fragmentManager
        this.containerId = containerId
    }

    override fun release() {
        fragmentManager = null
        containerId = null
    }

    override fun navigateTo(fragment: Fragment, addToBackstack: Boolean) {
        val fm = fragmentManager ?: return
        val container = containerId ?: return
        fm.beginTransaction()
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .replace(container, fragment)
            .apply {
                if (addToBackstack) {
                    addToBackStack(fragment::class.simpleName)
                }
            }
            .commit()
    }

    override fun navigateBack() {
        fragmentManager?.popBackStack()
    }
}