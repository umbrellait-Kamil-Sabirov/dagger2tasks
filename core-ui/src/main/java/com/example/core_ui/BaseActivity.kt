package com.example.core_ui

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity()