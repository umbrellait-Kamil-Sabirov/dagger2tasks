package com.example.core_ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class LoadingRecyclerViewScrollListener(
    private val layoutManager: LinearLayoutManager,
    private val IShouldLoadListener: IShouldLoadListener, private val offsetToStartLoading: Int
) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy > 0) {
            val firstVisible = layoutManager.findFirstVisibleItemPosition()
            val visibleCount = layoutManager.childCount
            val totalCount = layoutManager.itemCount

            if (firstVisible >= 0 && visibleCount + firstVisible + offsetToStartLoading >= totalCount)
                IShouldLoadListener.onShouldLoadMore()
        }
    }
}