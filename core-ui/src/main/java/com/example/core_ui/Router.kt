package com.example.core_ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

interface Router{
    fun navigateTo(fragment: Fragment, addToBackstack: Boolean)
    fun navigateBack()
    fun setUp(fragmentManager: FragmentManager, containerId: Int)
    fun release()
}